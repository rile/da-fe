import { createApp } from 'vue'
import App from './App.vue'
import router from './router'

// add bootstrap 5 styles and JS
import "bootstrap/dist/css/bootstrap.min.css"
import "bootstrap"
import 'bootstrap-icons/font/bootstrap-icons.css'

// add custom font and styles
import './assets/style.css';
import './assets/base.css';

// axios globally
import axios from "axios"
window.axios = axios

createApp(App).use(router).mount('#app')
