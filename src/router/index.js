import { createRouter, createWebHistory } from 'vue-router'

import Home from '@/components/Home.vue'
import List from '@/components/List.vue'
import NotFound from "@/components/NotFound.vue";

const routes = [
    {
        path: '/',
        name: 'Home',
        component: Home
    },
    {
        path: '/list',
        name: 'List',
        component: List
    },
    {
        path: '/:catchAll(.*)',
        name: "NotFound",
        component: NotFound
    }
]

const router = createRouter({
    history: createWebHistory(),
    routes,
    linkActiveClass: "active"
})

export default router
