# digital-atrium-fe

This template should help get you started developing with Vue 3 in Vite.

# Project Setup

## LOCALLY

Copy .env.example to .env.development and .env.production and populate data

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Compile and Minify for Production

```sh
npm run build
```

Access `http:localhost:3000`

## VIA DOCKER

Copy .env.example to .env.development and .env.production and populate data

```
docker build -t da_fe .
```
```
docker run -p 3000:8080 da_fe
```

Access `http:localhost:3000`
